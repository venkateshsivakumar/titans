/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AdministrativeRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.LogisticsEnterprise;
import Business.Network.Network;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.FoodRecoveryRequestByDonator;
import Business.WorkQueue.FoodRecoveryRequestToLogistics;
import Business.WorkQueue.WorkRequest;
import Business.WorkQueue.*;
import java.awt.CardLayout;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author navee
 */
public class FoodBankManageRequestsJPanel extends javax.swing.JPanel {
    
    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private Network network;
    private UserAccount userAccount;
    private EcoSystem ecosystem;
    
    /**
     * Creates new form FoodBankViewServicesJPanel
     */
    public FoodBankManageRequestsJPanel(JPanel userProcessContainer, Enterprise enterprise, Network network, UserAccount userAccount, EcoSystem ecosystem) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.network = network;
        this.userAccount = userAccount;
        this.ecosystem = ecosystem;
        lblUserName.setText(userAccount.getUsername());
        lblDateToday.setText(userAccount.toString());
        populateRequestByDonatorTable();
        populateRequestByDistributionTable();
    }
    
    public void populateRequestByDistributionTable() {
        
        DefaultTableModel model = (DefaultTableModel) tblFoodDelivertRequestByDistribution.getModel();

        model.setRowCount(0);

        for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : net.getEnterpriseDirectory().getEnterpriseList()) {
                if (ent.getEnterpriseType().getValue().equalsIgnoreCase("FoodBank")) {
                    for (WorkRequest request : ent.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodRequestByDCWorkReq) {
                            if(((FoodRequestByDCWorkReq) request).getEntname().equals(enterprise.getName())){
                                Object[] row = new Object[12];

                                row[0] = ((FoodRequestByDCWorkReq)request);
                                row[1] = ((FoodRequestByDCWorkReq)request).getSender();
                                row[2] =((FoodRequestByDCWorkReq)request).getReceiver() == null ? null : ((FoodRequestByDCWorkReq) request).getReceiver().getEmployee().getName();
                                row[3] = ((FoodRequestByDCWorkReq)request).getRequestDate();
                                row[4] = ((FoodRequestByDCWorkReq)request).getResolveDate();


                                row[5] = ((FoodRequestByDCWorkReq) request).getEntname();
                                row[6] = ((FoodRequestByDCWorkReq) request).getNoofmeals();
                                row[7] = ((FoodRequestByDCWorkReq) request).getMealtype();
                                row[8] = ((FoodRequestByDCWorkReq) request).getLoc();
                                row[9] = ((FoodRequestByDCWorkReq) request).getRequestingDate();
                                //row[10] = ((FoodRequestByDCWorkReq) request).getResolveDate();
                                row[10] = ((FoodRequestByDCWorkReq) request).getStatus();


                                model.addRow(row);
                            }
                        }
                    }

                }
            }
        }
        
        
    }
    
    public void populateRequestByDonatorTable() {
        DefaultTableModel model = (DefaultTableModel) foodRecoveryRequestByDonatorTbl.getModel();

        model.setRowCount(0);

        for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : net.getEnterpriseDirectory().getEnterpriseList()) {
                if (ent.getEnterpriseType().getValue().equalsIgnoreCase("FoodBank")) {
                    for (WorkRequest request : ent.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodRecoveryRequestByDonator) {
                            Object[] row = new Object[10];
                            row[0] = ((FoodRecoveryRequestByDonator) request);
                            //[1] = ((FoodRecoveryRequestToDriver) request).getRequestingDate();
                            row[1] = ((FoodRecoveryRequestByDonator) request).getSender();
                            row[2] = ((FoodRecoveryRequestByDonator) request).getReceiver();
                            row[3] = ((FoodRecoveryRequestByDonator) request).getRequestDate();
                            row[4] = ((FoodRecoveryRequestByDonator) request).getResolveDate();
                            row[5] = ((FoodRecoveryRequestByDonator) request).getFoodDescription();
                            row[6] = ((FoodRecoveryRequestByDonator) request).getTotalQuantity();
                            row[7] = ((FoodRecoveryRequestByDonator) request).getAddress();
                            //row[6] = net;
                            row[8] = ((FoodRecoveryRequestByDonator) request).getStatus();
                            row[9] = ((FoodRecoveryRequestByDonator) request).getMessage();
           // String result = ((StaffWorkRequest) request).getTestResult();
                            //row[3] = result == null ? "Waiting" : result;

                            model.addRow(row);
                        }
                    }

                }
            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        lblUserName = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblDateToday = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        foodRecoveryRequestByDonatorTbl = new javax.swing.JTable();
        txtComments = new javax.swing.JTextField();
        btnForwardToLogistics = new javax.swing.JButton();
        btnCancelDonatorRequest = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtComments1 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        btnForwardToLogistics1 = new javax.swing.JButton();
        btnCancelDistributionCenterRequest = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        btnBack = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblFoodDelivertRequestByDistribution = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Logged in as: ");

        lblUserName.setText("<username here>");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date:");

        lblDateToday.setText("<date here>");

        foodRecoveryRequestByDonatorTbl.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested date", "Accepted Date", "Food items", "Quantity", "Address", "Status", "Comments"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        foodRecoveryRequestByDonatorTbl.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(foodRecoveryRequestByDonatorTbl);

        txtComments.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCommentsActionPerformed(evt);
            }
        });

        btnForwardToLogistics.setText("Accept and request logistics");
        btnForwardToLogistics.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnForwardToLogisticsActionPerformed(evt);
            }
        });

        btnCancelDonatorRequest.setText("Cancel request");
        btnCancelDonatorRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelDonatorRequestActionPerformed(evt);
            }
        });

        jLabel1.setText("Comments");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Food Request By Distrubtion Centers");

        txtComments1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtComments1ActionPerformed(evt);
            }
        });

        jLabel5.setText("Comments:");

        btnForwardToLogistics1.setText("Accept and request logistics");
        btnForwardToLogistics1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnForwardToLogistics1ActionPerformed(evt);
            }
        });

        btnCancelDistributionCenterRequest.setText("Cancel request");
        btnCancelDistributionCenterRequest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelDistributionCenterRequestActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Food recovery requests by Donators");

        btnBack.setText("<<back");
        btnBack.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        tblFoodDelivertRequestByDistribution.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested Date", "Accepted Date", "Food Bank Name", "No of meals", "Meal Type", "Location", "Requesting Food for Date", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(tblFoodDelivertRequestByDistribution);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGap(262, 262, 262)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(txtComments1, javax.swing.GroupLayout.PREFERRED_SIZE, 611, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(38, 38, 38))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 85, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(btnForwardToLogistics1)
                                            .addGap(54, 54, 54)
                                            .addComponent(btnCancelDistributionCenterRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(331, 331, 331))))
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(txtComments, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 611, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 1004, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel2))
                                    .addGap(18, 18, 18)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(lblUserName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(lblDateToday, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(layout.createSequentialGroup()
                            .addContainerGap()
                            .addComponent(btnForwardToLogistics)
                            .addGap(93, 93, 93)
                            .addComponent(btnCancelDonatorRequest, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(271, 271, 271)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(291, 291, 291)
                                .addComponent(jLabel1)))))
                .addGap(20, 20, 20))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 989, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(35, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblUserName))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblDateToday)
                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(36, 36, 36)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtComments, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(41, 41, 41)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnForwardToLogistics)
                    .addComponent(btnCancelDonatorRequest))
                .addGap(26, 26, 26)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(217, 217, 217)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(txtComments1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnCancelDistributionCenterRequest)
                    .addComponent(btnForwardToLogistics1))
                .addGap(53, 53, 53)
                .addComponent(btnBack)
                .addContainerGap(294, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(501, 501, 501)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 129, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(502, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtCommentsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCommentsActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCommentsActionPerformed

    private void btnForwardToLogisticsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnForwardToLogisticsActionPerformed
        int selectedRow = foodRecoveryRequestByDonatorTbl.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }

        FoodRecoveryRequestByDonator foodRecoveryRequestByDonator = (FoodRecoveryRequestByDonator) foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 0);
        if (foodRecoveryRequestByDonator.getStatus().equalsIgnoreCase("Delivery Requested") || foodRecoveryRequestByDonator.getStatus().equalsIgnoreCase("Recovery Requested")
                ||foodRecoveryRequestByDonator.getStatus().equalsIgnoreCase("Cancelled")) {

            JOptionPane.showMessageDialog(null, "Can't request to Logistics");
        }else if(!foodRecoveryRequestByDonator.getStatus().equals("Accepted")) {
            //if (foodRecoveryRequestByDonator.getReceiver().getUsername().equalsIgnoreCase(userAccount.getUsername())) {

                FoodRecoveryRequestToLogistics foodRecoveryRequestToLogistics = new FoodRecoveryRequestToLogistics();
                String networkType;

                System.out.println(foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 5).toString());

                /*if (network.getName() == foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 6)) {
                    networkType = "IN";
                } else {
                    networkType = "OUT";
                }*/

                foodRecoveryRequestToLogistics.setRequestDate((Date) foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 3));
                foodRecoveryRequestToLogistics.setAcceptedDate((Date) foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 4));
                
                foodRecoveryRequestToLogistics.setSender(userAccount);
                foodRecoveryRequestToLogistics.setReceiver(null);
                //foodRecoveryByFoodBankWorkRequest.setRequestDate(new Date());
                foodRecoveryRequestToLogistics.setAcceptedDate(new Date());
                foodRecoveryRequestToLogistics.setResolveDate(null);
                foodRecoveryRequestToLogistics.setFoodItem((String)foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 5));
                foodRecoveryRequestToLogistics.setQuantity((String)foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 6));
                foodRecoveryRequestToLogistics.setAddress((String) foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 7));
                foodRecoveryRequestToLogistics.setMessage(foodRecoveryRequestByDonator.getMessage()+"\n"+txtComments1.getText());
                foodRecoveryRequestToLogistics.setStatus("Recovery Requested");
                foodRecoveryRequestByDonator.setStatus("Accepted");
                foodRecoveryRequestByDonator.setReceiver(userAccount);
                foodRecoveryRequestByDonator.setResolveDate(new Date());
                
                Enterprise ent = null;
                for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                    if (enterprise instanceof LogisticsEnterprise) {
                        ent = enterprise;
                        break;
                    }
                }
                if (ent != null) {
                    ent.getWorkQueue().getWorkRequestList().add(foodRecoveryRequestToLogistics);
                    userAccount.getWorkQueue().getWorkRequestList().add(foodRecoveryRequestToLogistics);
                    //System.out.println();
                }
                
                
                
                /*Organization org = null;
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    if (organization instanceof LogisticsOrganization) {
                        org = organization;
                        break;
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(foodRecoveryRequestToLogistics);
                    userAccount.getWorkQueue().getWorkRequestList().add(foodRecoveryRequestToLogistics);
                    JOptionPane.showMessageDialog(null, "Food Recovery has been requested");
                }*/

            //} 
        }

        populateRequestByDonatorTable();
    }//GEN-LAST:event_btnForwardToLogisticsActionPerformed

    private void btnCancelDonatorRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelDonatorRequestActionPerformed
        // TODO add your handling code here:
        
         int selectedRow = foodRecoveryRequestByDonatorTbl.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }

        FoodRecoveryRequestByDonator foodRecoveryRequestByDonator =  (FoodRecoveryRequestByDonator)foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 0);
        
        if (foodRecoveryRequestByDonator.getStatus().equalsIgnoreCase("Accepted")) {
            JOptionPane.showMessageDialog(null, "Can't cancel now, request is already with Logistics Enterprise");
                

        

        populateRequestByDonatorTable();
        } else {
            //if (foodRecoveryRequestByDonator.getReceiver().getUsername().equalsIgnoreCase(userAccount.getUsername())) {
            foodRecoveryRequestByDonator.setStatus("Cancelled");
            foodRecoveryRequestByDonator.setReceiver(userAccount);
             populateRequestByDonatorTable();
            

        }
    }//GEN-LAST:event_btnCancelDonatorRequestActionPerformed

    private void txtComments1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtComments1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtComments1ActionPerformed

    private void btnForwardToLogistics1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnForwardToLogistics1ActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblFoodDelivertRequestByDistribution.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }
            FoodRequestByDCWorkReq foodRequestByDCWorkReq = (FoodRequestByDCWorkReq)tblFoodDelivertRequestByDistribution.getValueAt(selectedRow, 0);
        //FoodRequestByDistributionCenter foodRequestByDistributionCenter = (FoodRequestByDistributionCenter) tblFoodDelivertRequestByDistribution.getValueAt(selectedRow, 0);
        if (foodRequestByDCWorkReq.getStatus().equalsIgnoreCase("Cancelled")) {

            JOptionPane.showMessageDialog(null, "Can't request to Logistics, Request is cancelled");
        }else if (foodRequestByDCWorkReq.getStatus().equalsIgnoreCase("Logistics team working on it")) {

            JOptionPane.showMessageDialog(null, "Can't complete the action");
        } else {
            //if (foodRecoveryRequestByDonator.getReceiver().getUsername().equalsIgnoreCase(userAccount.getUsername())) {

                FoodDeliveryRequestToLogistics foodDeliveryRequestToLogistics = new FoodDeliveryRequestToLogistics();
                //FoodRequestByDCWorkReq foodRequestByDCWorkReq = (FoodRequestByDCWorkReq)tblFoodDelivertRequestByDistribution.getValueAt(selectedRow, 0);

                //System.out.println(foodRecoveryRequestByDonatorTbl.getValueAt(selectedRow, 5).toString());

 
                foodDeliveryRequestToLogistics.setRequestDate(new Date());
                foodDeliveryRequestToLogistics.setAcceptedDate(null);
                
                foodDeliveryRequestToLogistics.setSender(userAccount);
                foodRequestByDCWorkReq.setReceiver(userAccount);
                foodDeliveryRequestToLogistics.setRequestingDate(((FoodRequestByDCWorkReq) foodRequestByDCWorkReq).getRequestingDate());
                foodRequestByDCWorkReq.setResolveDate(new Date());
                foodDeliveryRequestToLogistics.setResolveDate(null);
                foodDeliveryRequestToLogistics.setLoc(foodRequestByDCWorkReq.getLoc());
                foodDeliveryRequestToLogistics.setMealtype(foodRequestByDCWorkReq.getMealtype());
                foodDeliveryRequestToLogistics.setMessage(foodRequestByDCWorkReq.getMessage()+";"+txtComments1.getText());
                foodRequestByDCWorkReq.setStatus("Logistics Team working on it");
                foodDeliveryRequestToLogistics.setNoofmeals(foodRequestByDCWorkReq.getNoofmeals());
                foodDeliveryRequestToLogistics.setStatus("Delivery Requested");
                foodDeliveryRequestToLogistics.setNoofmeals(foodRequestByDCWorkReq.getNoofmeals());
                foodDeliveryRequestToLogistics.setReceiver(null);
                foodDeliveryRequestToLogistics.setEntname(((FoodRequestByDCWorkReq) foodRequestByDCWorkReq).getEntname());
                
                
                Enterprise ent = null;
                for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                    if (enterprise instanceof LogisticsEnterprise) {
                        ent = enterprise;
                        break;
                    }
                }
                if (ent != null) {
                    ent.getWorkQueue().getWorkRequestList().add(foodDeliveryRequestToLogistics);
                    userAccount.getWorkQueue().getWorkRequestList().add(foodDeliveryRequestToLogistics);
                    //System.out.println();
                }
                

        }

        populateRequestByDistributionTable();
    }//GEN-LAST:event_btnForwardToLogistics1ActionPerformed

    private void btnCancelDistributionCenterRequestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelDistributionCenterRequestActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblFoodDelivertRequestByDistribution.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }

        FoodRequestByDCWorkReq foodRequestByDCWorkReq = (FoodRequestByDCWorkReq)tblFoodDelivertRequestByDistribution.getValueAt(selectedRow, 0);
        
        if (foodRequestByDCWorkReq.getStatus().equalsIgnoreCase("Accepted") || foodRequestByDCWorkReq.getStatus().equalsIgnoreCase("Logistics team working on it")) {
            JOptionPane.showMessageDialog(null, "Can't cancel now, request is already with Logistics Enterprise");
                

        

        populateRequestByDistributionTable();
        } else {
            //if (foodRecoveryRequestByDonator.getReceiver().getUsername().equalsIgnoreCase(userAccount.getUsername())) {
            foodRequestByDCWorkReq.setStatus("Cancelled");
            foodRequestByDCWorkReq.setReceiver(userAccount);
             populateRequestByDistributionTable();
            

        }
    }//GEN-LAST:event_btnCancelDistributionCenterRequestActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);

    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnCancelDistributionCenterRequest;
    private javax.swing.JButton btnCancelDonatorRequest;
    private javax.swing.JButton btnForwardToLogistics;
    private javax.swing.JButton btnForwardToLogistics1;
    private javax.swing.JTable foodRecoveryRequestByDonatorTbl;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblDateToday;
    private javax.swing.JLabel lblUserName;
    private javax.swing.JTable tblFoodDelivertRequestByDistribution;
    private javax.swing.JTextField txtComments;
    private javax.swing.JTextField txtComments1;
    // End of variables declaration//GEN-END:variables


}
