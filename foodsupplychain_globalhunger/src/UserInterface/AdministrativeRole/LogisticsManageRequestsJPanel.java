/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.AdministrativeRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.LogisticsOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.FoodDeliveryRequestToDriver;
import Business.WorkQueue.FoodDeliveryRequestToLogistics;
import Business.WorkQueue.FoodRecoveryRequestByDonator;
import Business.WorkQueue.FoodRecoveryRequestToDriver;
import Business.WorkQueue.FoodRecoveryRequestToLogistics;
import Business.WorkQueue.FoodRequestByDCWorkReq;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author venk
 */
public class LogisticsManageRequestsJPanel extends javax.swing.JPanel {

    private JPanel userProcessContainer;
    private Enterprise enterprise;
    private Network network;
    private UserAccount userAccount;
    private EcoSystem ecosystem;
    
    /**
     * Creates new form ManageOrganizationJPanel
     */
    public LogisticsManageRequestsJPanel(JPanel userProcessContainer, Enterprise enterprise, Network network, UserAccount userAccount, EcoSystem ecosystem) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.network = network;
        this.userAccount = userAccount;
        this.ecosystem = ecosystem;

        /*if (enterprise.getOrganizationDirectory().getOrganizationList().isEmpty()) {
            requestDelBtn.setEnabled(false);
        }*/
        //ecosystem = ConfigureASystem.configure();
        populateRequestByFoodBankTable();
        populateRequestByFoodBankTableForDelivery();
        //valueLabel.setText(enterprise.getName());
    }
    
   public void populateRequestByFoodBankTable() {
        DefaultTableModel model = (DefaultTableModel) foodRecoveryRequestJTable.getModel();

        model.setRowCount(0);

        for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : net.getEnterpriseDirectory().getEnterpriseList()) {
                if (ent.getEnterpriseType().getValue().equalsIgnoreCase("Logistics")) {
                    for (WorkRequest request : ent.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodRecoveryRequestToLogistics) {
                            Object[] row = new Object[10];
                            row[0] = ((FoodRecoveryRequestToLogistics) request);
                            //[1] = ((FoodRecoveryRequestToDriver) request).getRequestingDate();
                            row[1] = ((FoodRecoveryRequestToLogistics) request).getSender();
                            row[2] = ((FoodRecoveryRequestToLogistics) request).getReceiver() == null ? null : ((FoodRecoveryRequestToLogistics) request).getReceiver().getEmployee().getName();
                            row[3] = ((FoodRecoveryRequestToLogistics) request).getRequestDate();
                            row[4] = ((FoodRecoveryRequestToLogistics) request).getResolveDate();
                            row[5] = ((FoodRecoveryRequestToLogistics) request).getFoodItem();
                            row[6] = ((FoodRecoveryRequestToLogistics) request).getQuantity();
                            row[7] = ((FoodRecoveryRequestToLogistics) request).getAddress();
                            //row[6] = net;
                            row[8] = ((FoodRecoveryRequestToLogistics) request).getStatus();
                            row[9] = ((FoodRecoveryRequestToLogistics) request).getMessage();
           // String result = ((StaffWorkRequest) request).getTestResult();
                            //row[3] = result == null ? "Waiting" : result;

                            model.addRow(row);
                        }
                    }

                }
            }
        }
    }
   
   public void populateRequestByFoodBankTableForDelivery() {
        DefaultTableModel model = (DefaultTableModel) foodDeliveryRequestJTable.getModel();

        model.setRowCount(0);

        for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : net.getEnterpriseDirectory().getEnterpriseList()) {
                if (ent.getEnterpriseType().getValue().equalsIgnoreCase("Logistics")) {
                    for (WorkRequest request : ent.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodDeliveryRequestToLogistics) {
                            Object[] row = new Object[11];
                            row[0] = ((FoodDeliveryRequestToLogistics)request);
                                row[1] = ((FoodDeliveryRequestToLogistics)request).getSender();
                                row[2] =((FoodDeliveryRequestToLogistics)request).getReceiver() == null ? null : ((FoodDeliveryRequestToLogistics) request).getReceiver().getEmployee().getName();
                                row[3] = ((FoodDeliveryRequestToLogistics)request).getRequestDate();
                                row[4] = ((FoodDeliveryRequestToLogistics)request).getResolveDate();


                                row[5] = ((FoodDeliveryRequestToLogistics) request).getEntname();
                                row[6] = ((FoodDeliveryRequestToLogistics) request).getNoofmeals();
                                row[7] = ((FoodDeliveryRequestToLogistics) request).getMealtype();
                                row[8] = ((FoodDeliveryRequestToLogistics) request).getLoc();
                                row[9] = ((FoodDeliveryRequestToLogistics) request).getRequestingDate();
                                row[10] = ((FoodDeliveryRequestToLogistics) request).getStatus();

                            model.addRow(row);
                        }
                    }

                }
            }
        }
    }

   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        cancelBtn1 = new javax.swing.JButton();
        refreshJButton = new javax.swing.JButton();
        backBtn = new javax.swing.JButton();
        requestRecoveryBtn = new javax.swing.JButton();
        enterpriseLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        foodRecoveryRequestJTable = new javax.swing.JTable();
        enterpriseLabel2 = new javax.swing.JLabel();
        refreshJButton1 = new javax.swing.JButton();
        requestDeliveryBtn = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        foodDeliveryRequestJTable = new javax.swing.JTable();
        cancelBtn = new javax.swing.JButton();
        deliveryCancelBtn = new javax.swing.JButton();

        cancelBtn1.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        cancelBtn1.setText("Cancel Request");
        cancelBtn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtn1ActionPerformed(evt);
            }
        });

        setBackground(new java.awt.Color(255, 255, 255));

        refreshJButton.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        refreshJButton.setText("Refresh Table");
        refreshJButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshJButtonActionPerformed(evt);
            }
        });

        backBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        backBtn.setText("<< Back");
        backBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                backBtnActionPerformed(evt);
            }
        });

        requestRecoveryBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        requestRecoveryBtn.setText("Accept And Request Food Recovery");
        requestRecoveryBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestRecoveryBtnActionPerformed(evt);
            }
        });

        enterpriseLabel1.setFont(new java.awt.Font("Broadway", 1, 24)); // NOI18N
        enterpriseLabel1.setText("Food Recovery Request");

        foodRecoveryRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested date", "Accepted Date", "Food items", "Quantity", "Address", "Status", "Comments"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        foodRecoveryRequestJTable.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(foodRecoveryRequestJTable);

        enterpriseLabel2.setFont(new java.awt.Font("Broadway", 1, 24)); // NOI18N
        enterpriseLabel2.setText("Food Delivery Request");

        refreshJButton1.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        refreshJButton1.setText("Refresh Table");
        refreshJButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                refreshJButton1ActionPerformed(evt);
            }
        });

        requestDeliveryBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        requestDeliveryBtn.setText("Accept And Request Food Delivery");
        requestDeliveryBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                requestDeliveryBtnActionPerformed(evt);
            }
        });

        foodDeliveryRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested Date", "Accepted Date", "Food Bank Name", "No of meals", "Meal Type", "Location", "Requesting Food for Date", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(foodDeliveryRequestJTable);

        cancelBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        cancelBtn.setText("Cancel Request");
        cancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cancelBtnActionPerformed(evt);
            }
        });

        deliveryCancelBtn.setFont(new java.awt.Font("Lucida Grande", 1, 14)); // NOI18N
        deliveryCancelBtn.setText("Cancel Request");
        deliveryCancelBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deliveryCancelBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(refreshJButton1)
                .addGap(48, 48, 48)
                .addComponent(requestDeliveryBtn)
                .addGap(64, 64, 64)
                .addComponent(deliveryCancelBtn)
                .addGap(58, 58, 58)
                .addComponent(backBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 89, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(69, 69, 69))
            .addGroup(layout.createSequentialGroup()
                .addGap(400, 400, 400)
                .addComponent(enterpriseLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 1001, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1004, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGap(400, 400, 400)
                        .addComponent(enterpriseLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 354, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(19, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(379, 379, 379)
                .addComponent(refreshJButton)
                .addGap(18, 18, 18)
                .addComponent(requestRecoveryBtn)
                .addGap(18, 18, 18)
                .addComponent(cancelBtn)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(enterpriseLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(requestRecoveryBtn)
                    .addComponent(cancelBtn)
                    .addComponent(refreshJButton))
                .addGap(20, 20, 20)
                .addComponent(enterpriseLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(refreshJButton1)
                    .addComponent(requestDeliveryBtn)
                    .addComponent(backBtn)
                    .addComponent(deliveryCancelBtn)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void refreshJButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshJButtonActionPerformed
        populateRequestByFoodBankTable();
    }//GEN-LAST:event_refreshJButtonActionPerformed

    private void backBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_backBtnActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
    }//GEN-LAST:event_backBtnActionPerformed

    private void requestRecoveryBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestRecoveryBtnActionPerformed

        int selectedRow = foodRecoveryRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }

        //System.out.println(foodRecoveryRequestJTable.getValueAt(selectedRow, 0));

       // FoodRecoveryRequestToLogistics request = (FoodRecoveryRequestToLogistics) foodRecoveryRequestJTable.getValueAt(selectedRow, 0);
       /* if (request.getStatus().equalsIgnoreCase("Accepted")) {

            JOptionPane.showMessageDialog(null, "Request already acknowledged");
        } else {
            request.setReceiver(userAccount);
            request.setResolveDate(new Date());
            request.setStatus("Accepted");
            populateRequestByFoodBankTable();
        }*/

        FoodRecoveryRequestToLogistics foodRecoveryRequestToLogistics = (FoodRecoveryRequestToLogistics) foodRecoveryRequestJTable.getValueAt(selectedRow, 0);
        if (foodRecoveryRequestToLogistics.getStatus().equalsIgnoreCase("Accepted") || foodRecoveryRequestToLogistics.getStatus().equalsIgnoreCase("Cancelled")) {

            JOptionPane.showMessageDialog(null, "Can't request Pickup");
        } else {
            //if (foodRecoveryRequestToLogistics.getReceiver().getUsername().equalsIgnoreCase(userAccount.getUsername())) {

                FoodRecoveryRequestToDriver foodRecoveryRequestToDriver = new FoodRecoveryRequestToDriver();
                String networkType;

                System.out.println(foodRecoveryRequestJTable.getValueAt(selectedRow, 5).toString());

                /*if (network.getName() == foodRecoveryRequestJTable.getValueAt(selectedRow, 6)) {
                    networkType = "IN";
                } else {
                    networkType = "OUT";
                }*/

                foodRecoveryRequestToDriver.setRequestDate(new Date());
                //foodRecoveryRequestToDriver.setAcceptedDate(new Date());
                
                foodRecoveryRequestToDriver.setSender(userAccount);
                foodRecoveryRequestToDriver.setReceiver(null);
                //foodRecoveryByFoodBankWorkRequest.setRequestDate(new Date());
                foodRecoveryRequestToDriver.setAcceptedDate(new Date());
                foodRecoveryRequestToDriver.setResolveDate(null);
                foodRecoveryRequestToDriver.setFoodItem(foodRecoveryRequestToLogistics.getFoodItem());
                foodRecoveryRequestToDriver.setQuantity(foodRecoveryRequestToLogistics.getQuantity());
                foodRecoveryRequestToDriver.setAddress(foodRecoveryRequestToLogistics.getAddress());
                foodRecoveryRequestToDriver.setMessage(foodRecoveryRequestToLogistics.getMessage());
                foodRecoveryRequestToDriver.setStatus("Recovery Requested");
                foodRecoveryRequestToLogistics.setStatus("Accepted");
                foodRecoveryRequestToLogistics.setReceiver(userAccount);
                foodRecoveryRequestToLogistics.setResolveDate(new Date());
                Organization org = null;
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    if (organization instanceof LogisticsOrganization) {
                        org = organization;
                        break;
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(foodRecoveryRequestToDriver);
                    userAccount.getWorkQueue().getWorkRequestList().add(foodRecoveryRequestToDriver);
                    JOptionPane.showMessageDialog(null, "Recovery has been requested");
                }

            //} 
        }

        populateRequestByFoodBankTable();
    }//GEN-LAST:event_requestRecoveryBtnActionPerformed

    private void refreshJButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_refreshJButton1ActionPerformed
       populateRequestByFoodBankTableForDelivery();
    }//GEN-LAST:event_refreshJButton1ActionPerformed

    private void requestDeliveryBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_requestDeliveryBtnActionPerformed
       int selectedRow = foodDeliveryRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }

        //System.out.println(foodRecoveryRequestJTable.getValueAt(selectedRow, 0));

       // FoodRecoveryRequestToLogistics request = (FoodRecoveryRequestToLogistics) foodRecoveryRequestJTable.getValueAt(selectedRow, 0);
       /* if (request.getStatus().equalsIgnoreCase("Accepted")) {

            JOptionPane.showMessageDialog(null, "Request already acknowledged");
        } else {
            request.setReceiver(userAccount);
            request.setResolveDate(new Date());
            request.setStatus("Accepted");
            populateRequestByFoodBankTable();
        }*/

        FoodDeliveryRequestToLogistics foodDeliveryRequestToLogistics = (FoodDeliveryRequestToLogistics) foodDeliveryRequestJTable.getValueAt(selectedRow, 0);
        if ("Accepted".equalsIgnoreCase(foodDeliveryRequestToLogistics.getStatus()) || "Cancelled".equalsIgnoreCase(foodDeliveryRequestToLogistics.getStatus())) {

            JOptionPane.showMessageDialog(null, "Can't request Pickup");
        } else {
            //if (foodRecoveryRequestToLogistics.getReceiver().getUsername().equalsIgnoreCase(userAccount.getUsername())) {

                FoodDeliveryRequestToDriver foodDeliveryRequestToDriver = new FoodDeliveryRequestToDriver();
                String networkType;

                foodDeliveryRequestToDriver.setRequestDate(new Date());
                foodDeliveryRequestToDriver.setAcceptedDate(null);
                
                foodDeliveryRequestToDriver.setSender(userAccount);
                foodDeliveryRequestToLogistics.setReceiver(userAccount);
                //foodRecoveryByFoodBankWorkRequest.setRequestDate(new Date());
                foodDeliveryRequestToLogistics.setResolveDate(new Date());
                foodDeliveryRequestToDriver.setResolveDate(null);
                foodDeliveryRequestToDriver.setEntname(foodDeliveryRequestToLogistics.getEntname());
                foodDeliveryRequestToDriver.setRequestDate(foodDeliveryRequestToLogistics.getRequestingDate());
                foodDeliveryRequestToDriver.setLoc(foodDeliveryRequestToLogistics.getLoc());
                foodDeliveryRequestToDriver.setMealtype(foodDeliveryRequestToLogistics.getMealtype());
                foodDeliveryRequestToDriver.setMessage(foodDeliveryRequestToLogistics.getMessage()/*+"\n"+txtComments1.getText()*/);
                foodDeliveryRequestToLogistics.setStatus("Accepted");
                foodDeliveryRequestToDriver.setNoofmeals(foodDeliveryRequestToLogistics.getNoofmeals());
                //foodDeliveryRequestToLogistics.setStatus("Delivery Requested");
                //foodDeliveryRequestToDriver.setNoofmeals(foodDeliveryRequestToLogistics.getNoofmeals());
                foodDeliveryRequestToDriver.setReceiver(null);
                foodDeliveryRequestToDriver.setRequestingDate(((FoodDeliveryRequestToLogistics) foodDeliveryRequestToLogistics).getRequestingDate());
                

                Organization org = null;
                for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()) {
                    if (organization instanceof LogisticsOrganization) {
                        org = organization;
                        break;
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(foodDeliveryRequestToDriver);
                    userAccount.getWorkQueue().getWorkRequestList().add(foodDeliveryRequestToDriver);
                    JOptionPane.showMessageDialog(null, "Recovery has been requested");
                }

            //} 
        }

        populateRequestByFoodBankTableForDelivery();
    }//GEN-LAST:event_requestDeliveryBtnActionPerformed

    private void cancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtnActionPerformed
        int selectedRow = foodRecoveryRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }

        FoodRecoveryRequestToLogistics foodRecoveryRequestToLogistics =  (FoodRecoveryRequestToLogistics)foodRecoveryRequestJTable.getValueAt(selectedRow, 0);
        
        if (foodRecoveryRequestToLogistics.getStatus().equalsIgnoreCase("Accepted")) {
            JOptionPane.showMessageDialog(null, "Can't cancel now, request is already with Logistics Enterprise");
 
        } 
        else {
            //if (foodRecoveryRequestByDonator.getReceiver().getUsername().equalsIgnoreCase(userAccount.getUsername())) {
            foodRecoveryRequestToLogistics.setStatus("Cancelled");
            //foodRecoveryRequestToLogistics.setResolveDate(new Date());
            foodRecoveryRequestToLogistics.setReceiver(userAccount);
             populateRequestByFoodBankTable();
        }
    }//GEN-LAST:event_cancelBtnActionPerformed

    private void cancelBtn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cancelBtn1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cancelBtn1ActionPerformed

    private void deliveryCancelBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deliveryCancelBtnActionPerformed
        int selectedRow = foodDeliveryRequestJTable.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }

        FoodDeliveryRequestToLogistics foodDeliveryRequestToLogistics = (FoodDeliveryRequestToLogistics) foodDeliveryRequestJTable.getValueAt(selectedRow, 0);
        
        if (foodDeliveryRequestToLogistics.getStatus().equalsIgnoreCase("Accepted")) {
            JOptionPane.showMessageDialog(null, "Can't cancel now, request is already with Logistics Enterprise");
 
        } 
        else {
            //if (foodRecoveryRequestByDonator.getReceiver().getUsername().equalsIgnoreCase(userAccount.getUsername())) {
            foodDeliveryRequestToLogistics.setStatus("Cancelled");
            //foodDeliveryRequestToLogistics.setRequestDate(new Date());
            foodDeliveryRequestToLogistics.setReceiver(userAccount);
             populateRequestByFoodBankTableForDelivery();
        }
    }//GEN-LAST:event_deliveryCancelBtnActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton backBtn;
    private javax.swing.JButton cancelBtn;
    private javax.swing.JButton cancelBtn1;
    private javax.swing.JButton deliveryCancelBtn;
    private javax.swing.JLabel enterpriseLabel1;
    private javax.swing.JLabel enterpriseLabel2;
    private javax.swing.JTable foodDeliveryRequestJTable;
    private javax.swing.JTable foodRecoveryRequestJTable;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JButton refreshJButton;
    private javax.swing.JButton refreshJButton1;
    private javax.swing.JButton requestDeliveryBtn;
    private javax.swing.JButton requestRecoveryBtn;
    // End of variables declaration//GEN-END:variables
}
