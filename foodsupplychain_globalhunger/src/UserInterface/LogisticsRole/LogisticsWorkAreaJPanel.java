/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.LogisticsRole;

import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.LogisticsOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.FoodDeliveryRequestToDriver;
import Business.WorkQueue.FoodRecoveryRequestToDriver;
import Business.WorkQueue.WorkRequest;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author venka
 */
public class LogisticsWorkAreaJPanel extends javax.swing.JPanel {
    
    private JPanel userProcessContainer;
    private LogisticsOrganization organization;
    private Enterprise enterprise;
    private UserAccount userAccount;
    private Network network;

    /**
     * Creates new form NewJPanel
     */
    public LogisticsWorkAreaJPanel(JPanel userProcessContainer, UserAccount account, LogisticsOrganization organization, Enterprise enterprise,Network network) {
        initComponents();
        
        this.userProcessContainer = userProcessContainer;
        this.organization = organization;
        this.enterprise = enterprise;
        this.network =network;
        this.userAccount = account;
        lblUserName.setText(userAccount.getUsername());
        lblDateToday.setText(" " + new Date());
        populateRequestTable();
        populateDeliveryRequestTable();
    }
    
    public void populateRequestTable(){
        DefaultTableModel model = (DefaultTableModel) tblWorkQueue.getModel();
        
        model.setRowCount(0);
        //for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                for(Organization org : ent.getOrganizationDirectory().getOrganizationList())
                {
                if (org.getName().equalsIgnoreCase("Logistics Staff Organization")) {
                    for (WorkRequest request : org.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodRecoveryRequestToDriver) {
            Object[] row = new Object[11];
            row[0] = ((FoodRecoveryRequestToDriver)request);
            row[1] = ((FoodRecoveryRequestToDriver)request).getSender();
            row[2] = ((FoodRecoveryRequestToDriver)request).getReceiver() == null ? null : ((FoodRecoveryRequestToDriver) request).getReceiver().getEmployee().getName();
            row[3] = ((FoodRecoveryRequestToDriver)request).getRequestDate();
            row[4] = ((FoodRecoveryRequestToDriver)request).getResolveDate();
            row[5] = ((FoodRecoveryRequestToDriver)request).getFoodItem();
            row[6] = ((FoodRecoveryRequestToDriver)request).getQuantity();
            row[7] = ((FoodRecoveryRequestToDriver)request).getAddress();
            row[8] = ((FoodRecoveryRequestToDriver)request).getStatus();
            row[10] = ((FoodRecoveryRequestToDriver)request).getMessage();
            row[9] = ((FoodRecoveryRequestToDriver)request).getDistance();

            model.addRow(row);
        }
                    }
                }
            }
            }
    }
    
     public void populateDeliveryRequestTable(){
        DefaultTableModel model = (DefaultTableModel) foodDeliveryRequestJTable.getModel();
        
        model.setRowCount(0);
        //for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                for(Organization org : ent.getOrganizationDirectory().getOrganizationList())
                {
                if (org.getName().equalsIgnoreCase("Logistics Staff Organization")) {
                    for (WorkRequest request : org.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodDeliveryRequestToDriver) {
                                Object[] row = new Object[11];
                                row[0] = ((FoodDeliveryRequestToDriver)request);
                                row[1] = ((FoodDeliveryRequestToDriver)request).getSender();
                                row[2] =((FoodDeliveryRequestToDriver)request).getReceiver() == null ? null : ((FoodDeliveryRequestToDriver) request).getReceiver().getEmployee().getName();
                                row[3] = ((FoodDeliveryRequestToDriver)request).getRequestDate();
                                row[4] = ((FoodDeliveryRequestToDriver)request).getAcceptedDate();


                                row[5] = ((FoodDeliveryRequestToDriver) request).getEntname();
                                row[6] = ((FoodDeliveryRequestToDriver) request).getNoofmeals();
                                row[7] = ((FoodDeliveryRequestToDriver) request).getMealtype();
                                row[8] = ((FoodDeliveryRequestToDriver) request).getLoc();
                                row[9] = ((FoodDeliveryRequestToDriver) request).getRequestingDate();
                                row[10] = ((FoodDeliveryRequestToDriver) request).getStatus();

            model.addRow(row);
        }
                    }
                }
            }
            }
    }
    

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblUserName = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblDateToday = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblWorkQueue = new javax.swing.JTable();
        btnPickedUpFromDonator = new javax.swing.JButton();
        btnDeliveredToFoodBank = new javax.swing.JButton();
        distanceTxt = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        distanceTxt1 = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        btnPickedUpFromFoodBank = new javax.swing.JButton();
        btnDeliveredToDistribution = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        foodDeliveryRequestJTable = new javax.swing.JTable();
        recoveryResetBtn = new javax.swing.JButton();
        deliveryResetBtn = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Logistics Staff Recovery Requests");

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Logged in as: ");

        lblUserName.setText("<username here>");

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Date:");

        lblDateToday.setText("<date here>");

        tblWorkQueue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested date", "Accepted Date", "Food items", "Quantity", "Address", "Status", "DistanceCovered", "Comments"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.Integer.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblWorkQueue.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblWorkQueue);

        btnPickedUpFromDonator.setText("Picked up");
        btnPickedUpFromDonator.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPickedUpFromDonatorActionPerformed(evt);
            }
        });

        btnDeliveredToFoodBank.setText("Completed");
        btnDeliveredToFoodBank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeliveredToFoodBankActionPerformed(evt);
            }
        });

        distanceTxt.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                distanceTxtActionPerformed(evt);
            }
        });

        jLabel5.setText("Distance Covered :");

        jLabel6.setText("(Miles)");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Logistics Staff Delivery Requests");

        jLabel8.setText("Distance Covered :");

        distanceTxt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                distanceTxt1ActionPerformed(evt);
            }
        });

        jLabel9.setText("(Miles)");

        btnPickedUpFromFoodBank.setText("Picked up");
        btnPickedUpFromFoodBank.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPickedUpFromFoodBankActionPerformed(evt);
            }
        });

        btnDeliveredToDistribution.setText("Completed");
        btnDeliveredToDistribution.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeliveredToDistributionActionPerformed(evt);
            }
        });

        foodDeliveryRequestJTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested Date", "Accepted Date", "Food Bank Name", "No of meals", "Meal Type", "Location", "Requesting Food for Date", "Status"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        jScrollPane3.setViewportView(foodDeliveryRequestJTable);

        recoveryResetBtn.setText("Reset");
        recoveryResetBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                recoveryResetBtnActionPerformed(evt);
            }
        });

        deliveryResetBtn.setText("Reset");
        deliveryResetBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deliveryResetBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(244, 244, 244)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1004, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblUserName, javax.swing.GroupLayout.DEFAULT_SIZE, 143, Short.MAX_VALUE)
                            .addComponent(lblDateToday, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(105, 105, 105))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnPickedUpFromDonator)
                                .addGap(18, 18, 18)
                                .addComponent(btnDeliveredToFoodBank)
                                .addGap(463, 463, 463))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(distanceTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(recoveryResetBtn)
                                .addGap(77, 77, 77))))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 471, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(349, 349, 349))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(282, 282, 282)
                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btnPickedUpFromFoodBank)
                        .addGap(18, 18, 18)
                        .addComponent(btnDeliveredToDistribution)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(distanceTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(deliveryResetBtn)
                        .addGap(72, 72, 72))))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 1001, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblUserName))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblDateToday))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(distanceTxt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(jLabel6)
                    .addComponent(recoveryResetBtn))
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPickedUpFromDonator)
                    .addComponent(btnDeliveredToFoodBank))
                .addGap(14, 14, 14)
                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(distanceTxt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(deliveryResetBtn))
                .addGap(35, 35, 35)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPickedUpFromFoodBank)
                    .addComponent(btnDeliveredToDistribution))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jLabel11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/UserInterface/LogisticsRole/images (1).jpg"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel11)
                .addGap(0, 50, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(236, 236, 236)
                .addComponent(jLabel11)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPickedUpFromDonatorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPickedUpFromDonatorActionPerformed
        int selectedRow = tblWorkQueue.getSelectedRow();

        if (selectedRow < 0) {
            return;
        }
        FoodRecoveryRequestToDriver foodRecoveryRequestToDriver = (FoodRecoveryRequestToDriver) tblWorkQueue.getValueAt(selectedRow, 0);
        if (foodRecoveryRequestToDriver.getStatus().equalsIgnoreCase("Pickedup")) {

            JOptionPane.showMessageDialog(null, "Can't request Pickup");
        } else {
                foodRecoveryRequestToDriver.setReceiver(userAccount);
                foodRecoveryRequestToDriver.setResolveDate(new Date());
                foodRecoveryRequestToDriver.setStatus("PickedUp");
                

        }

        populateRequestTable();
    }//GEN-LAST:event_btnPickedUpFromDonatorActionPerformed

    private void btnDeliveredToFoodBankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeliveredToFoodBankActionPerformed
        int selectedRow = tblWorkQueue.getSelectedRow();
        float distance =0;
        try{
        distance = Float.valueOf(distanceTxt.getText());
        if(distance<=0){
            JOptionPane.showMessageDialog(null, "Distance cannot be negative or zero");
            return;
        }
        }
        catch(NumberFormatException num){
            JOptionPane.showMessageDialog(null, "Please enter a valid number in distance");
            return;
        }
        if (selectedRow < 0) {
            return;
        }
        FoodRecoveryRequestToDriver foodRecoveryRequestToDriver = (FoodRecoveryRequestToDriver) tblWorkQueue.getValueAt(selectedRow, 0);
        if (foodRecoveryRequestToDriver.getStatus().equalsIgnoreCase("completed")) {

            JOptionPane.showMessageDialog(null, "Can't request Pickup");
        } else if(userAccount.equals(foodRecoveryRequestToDriver.getReceiver())){
                foodRecoveryRequestToDriver.setReceiver(userAccount);
                foodRecoveryRequestToDriver.setDistance(distance);
                foodRecoveryRequestToDriver.setResolveDate(new Date());
                foodRecoveryRequestToDriver.setStatus("Completed");
        }else{
            JOptionPane.showMessageDialog(null, "User Missmatch");
        }

        populateRequestTable();
    }//GEN-LAST:event_btnDeliveredToFoodBankActionPerformed

    private void distanceTxtActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_distanceTxtActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_distanceTxtActionPerformed

    private void distanceTxt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_distanceTxt1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_distanceTxt1ActionPerformed

    private void btnPickedUpFromFoodBankActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPickedUpFromFoodBankActionPerformed
        int selectedRow = foodDeliveryRequestJTable.getSelectedRow();
        float distance = 0;
        
        if (selectedRow < 0) {
            return;
        }
        FoodDeliveryRequestToDriver foodDeliveryRequestToDriver = (FoodDeliveryRequestToDriver) foodDeliveryRequestJTable.getValueAt(selectedRow, 0);
        if ("Pickedup".equalsIgnoreCase(foodDeliveryRequestToDriver.getStatus())) {

            JOptionPane.showMessageDialog(null, "Can't request Pickup");
        } else {
                foodDeliveryRequestToDriver.setReceiver(userAccount);
                foodDeliveryRequestToDriver.setAcceptedDate(new Date());
                foodDeliveryRequestToDriver.setStatus("PickedUp");
                foodDeliveryRequestToDriver.setAcceptedDate(new Date());
        }

        populateDeliveryRequestTable();
    }//GEN-LAST:event_btnPickedUpFromFoodBankActionPerformed

    private void btnDeliveredToDistributionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeliveredToDistributionActionPerformed
        int selectedRow = foodDeliveryRequestJTable.getSelectedRow();
        try{
        float distance = Float.valueOf(distanceTxt1.getText());
        if(distance<=0){
            JOptionPane.showMessageDialog(null, "Distance cannot be negative or zero");
            return;
        }
        }
        catch(NumberFormatException num){
            JOptionPane.showMessageDialog(null, "Please enter a valid number in distance");
            return;
        }
        if (selectedRow < 0) {
            return;
        }
        FoodDeliveryRequestToDriver foodDeliveryRequestToDriver = (FoodDeliveryRequestToDriver) foodDeliveryRequestJTable.getValueAt(selectedRow, 0);
        if ("completed".equalsIgnoreCase(foodDeliveryRequestToDriver.getStatus())) {

            JOptionPane.showMessageDialog(null, "Can't request Pickup");
        } else if(userAccount.equals(foodDeliveryRequestToDriver.getReceiver())) {
                foodDeliveryRequestToDriver.setReceiver(userAccount);
                foodDeliveryRequestToDriver.setDistance(Float.valueOf(distanceTxt1.getText()));
                foodDeliveryRequestToDriver.setStatus("Completed");
                foodDeliveryRequestToDriver.setAcceptedDate(new Date());
        } else {
                JOptionPane.showMessageDialog(null, "User Missmatch");
        }

        populateDeliveryRequestTable();
    }//GEN-LAST:event_btnDeliveredToDistributionActionPerformed

    private void recoveryResetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_recoveryResetBtnActionPerformed
       populateRequestTable();
    }//GEN-LAST:event_recoveryResetBtnActionPerformed

    private void deliveryResetBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deliveryResetBtnActionPerformed
        populateDeliveryRequestTable();
    }//GEN-LAST:event_deliveryResetBtnActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnDeliveredToDistribution;
    private javax.swing.JButton btnDeliveredToFoodBank;
    private javax.swing.JButton btnPickedUpFromDonator;
    private javax.swing.JButton btnPickedUpFromFoodBank;
    private javax.swing.JButton deliveryResetBtn;
    private javax.swing.JTextField distanceTxt;
    private javax.swing.JTextField distanceTxt1;
    private javax.swing.JTable foodDeliveryRequestJTable;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblDateToday;
    private javax.swing.JLabel lblUserName;
    private javax.swing.JButton recoveryResetBtn;
    private javax.swing.JTable tblWorkQueue;
    // End of variables declaration//GEN-END:variables
}
