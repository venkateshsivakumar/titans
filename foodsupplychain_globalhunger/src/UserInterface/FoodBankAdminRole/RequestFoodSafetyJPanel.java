/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package UserInterface.FoodBankAdminRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Enterprise.FoodBankEnterprise;
import Business.Network.Network;
import Business.Organization.*;
import Business.UserAccount.UserAccount;
import Business.WorkQueue.FoodDeliveryRequestToLogistics;
import Business.WorkQueue.FoodRecoveryRequestToDriver;
import Business.WorkQueue.FoodRequestByDistributionCenter;
import Business.WorkQueue.FoodSafetyCheckRequest;
import Business.WorkQueue.WorkRequest;
import java.awt.CardLayout;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author navee
 */
public class RequestFoodSafetyJPanel extends javax.swing.JPanel {

    
    JPanel userProcessContainer;
    Enterprise enterprise;
    Network network;
    UserAccount userAccount;
    EcoSystem ecosystem;
    /**
     * Creates new form RequestFoodSafetyJPanel
     */
    public RequestFoodSafetyJPanel(JPanel userProcessContainer, Enterprise enterprise, Network network, UserAccount userAccount, EcoSystem ecosystem) {
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.network = network;
        this.userAccount = userAccount;
        this.ecosystem = ecosystem;
        lblUsername.setText(userAccount.getUsername());
        lblDate.setText((new Date()).toString());
        
        populateRequestsTable();
        populateStatusTable();
        
    }
    
    public void populateRequestsTable(){
        
                DefaultTableModel model = (DefaultTableModel) tblWorkQueue.getModel();
        
        model.setRowCount(0);
        //for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                for(Organization org : ent.getOrganizationDirectory().getOrganizationList())
                {
                if (org.getName().equalsIgnoreCase("Logistics Staff Organization")) {
                    for (WorkRequest request : org.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodRecoveryRequestToDriver) {
                            if (request.getStatus().equalsIgnoreCase("completed")){
                                    Object[] row = new Object[10];
                                    row[0] = ((FoodRecoveryRequestToDriver)request);
                                    row[1] = ((FoodRecoveryRequestToDriver)request).getSender();
                                    row[2] =((FoodRecoveryRequestToDriver)request).getReceiver() == null ? null : ((FoodRecoveryRequestToDriver) request).getReceiver().getEmployee().getName();
                                    row[3] = ((FoodRecoveryRequestToDriver)request).getRequestDate();
                                    row[4] = ((FoodRecoveryRequestToDriver)request).getResolveDate();
                                    row[5] = ((FoodRecoveryRequestToDriver)request).getFoodItem();
                                    row[6] = ((FoodRecoveryRequestToDriver)request).getQuantity();
                                    row[7] = ((FoodRecoveryRequestToDriver)request).getAddress();
                                    row[8] = ((FoodRecoveryRequestToDriver)request).getStatus();
                                    row[9] = ((FoodRecoveryRequestToDriver)request).getMessage();

                                    model.addRow(row);
                            }
        }
                    }
                }
            }
            }
        
        
        
    }
    
        public void populateStatusTable(){
        
        DefaultTableModel model = (DefaultTableModel) tblWorkQueue1.getModel();
        
        model.setRowCount(0);
        //for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                for(Organization org : ent.getOrganizationDirectory().getOrganizationList())
                {
                if (org instanceof FoodSafetyOfficerOrganization) {
                    for (WorkRequest request : org.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodSafetyCheckRequest) {
                                    Object[] row = new Object[10];
                                    row[0] = (FoodSafetyCheckRequest)request;
                                    row[1] = ((FoodSafetyCheckRequest)request).getSender();
                                    row[2] =((FoodSafetyCheckRequest)request).getReceiver() == null ? null : ((FoodSafetyCheckRequest) request).getReceiver().getEmployee().getName();
                                    row[3] = ((FoodSafetyCheckRequest)request).getRequestDate();
                                    row[4] = ((FoodSafetyCheckRequest)request).getAcceptedDate();
                                    row[5] = ((FoodSafetyCheckRequest)request).getFoodDescription();
                                    row[6] = ((FoodSafetyCheckRequest)request).getTotalQuantity();
                                    row[7] = ((FoodSafetyCheckRequest)request).getResolveDate();
                                    row[8] = ((FoodSafetyCheckRequest)request).getStatus();
                                    
                                    row[9] = ((FoodSafetyCheckRequest)request).getMessage();

                                    model.addRow(row);
                            
        }
                    }
                }
            }
            }
        
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblUsername = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        btnRequestFoodSafetyCheck = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblWorkQueue = new javax.swing.JTable();
        txtComments = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblWorkQueue1 = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(255, 255, 255));

        jLabel1.setText("Logged in as:");

        lblUsername.setText("<username here>");

        jLabel3.setText("Date:");

        lblDate.setText("<Date here>");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Request Food Safety Check Responsibility");

        btnRequestFoodSafetyCheck.setText("Request Safety Check");
        btnRequestFoodSafetyCheck.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRequestFoodSafetyCheckActionPerformed(evt);
            }
        });

        btnBack.setText("<<back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        tblWorkQueue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested date", "Accepted Date", "Food items", "Quantity", "Address", "Status", "Comments"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, false, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblWorkQueue.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblWorkQueue);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel4.setText("Comments:");

        tblWorkQueue1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested date", "Accepted Date", "Food items", "Quantity", "Resolved Date", "Status", "Comments"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.Object.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, true, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblWorkQueue1.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tblWorkQueue1);

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Food Safety Requests status");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(308, 308, 308)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(89, 89, 89)
                        .addComponent(btnRequestFoodSafetyCheck))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(104, 104, 104)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 859, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 861, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(74, 74, 74)
                                .addComponent(txtComments, javax.swing.GroupLayout.PREFERRED_SIZE, 659, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap(113, Short.MAX_VALUE)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 585, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 109, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(39, 39, 39)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblUsername, javax.swing.GroupLayout.PREFERRED_SIZE, 149, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(205, 205, 205)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 585, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(34, 34, 34)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblUsername))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel3)
                            .addComponent(lblDate)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(32, 96, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnRequestFoodSafetyCheck)
                            .addComponent(btnBack))
                        .addGap(23, 23, 23))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(42, 42, 42)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtComments, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(65, 65, 65))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        userProcessContainer.remove(this);
        CardLayout layout = (CardLayout)userProcessContainer.getLayout();
        layout.previous(userProcessContainer);
        
        
    }//GEN-LAST:event_btnBackActionPerformed

    private void btnRequestFoodSafetyCheckActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRequestFoodSafetyCheckActionPerformed
        // TODO add your handling code here:
                int selectedRow = tblWorkQueue.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }

        FoodRecoveryRequestToDriver foodRecoveryRequestToDriver = (FoodRecoveryRequestToDriver) tblWorkQueue.getValueAt(selectedRow, 0);
        if(!foodRecoveryRequestToDriver.equals("Safety Check Requested")){
 
                FoodSafetyCheckRequest foodSafetyCheckRequest = new FoodSafetyCheckRequest();
                String networkType;

                System.out.println(tblWorkQueue.getValueAt(selectedRow, 5).toString());



                foodSafetyCheckRequest.setRequestDate(foodRecoveryRequestToDriver.getResolveDate());
                foodSafetyCheckRequest.setRequestDate(new Date());
                
                foodSafetyCheckRequest.setSender(userAccount);
                foodSafetyCheckRequest.setReceiver(null);
                foodSafetyCheckRequest.setResolveDate(null);
                foodSafetyCheckRequest.setFoodDescription(foodRecoveryRequestToDriver.getFoodItem());
                foodSafetyCheckRequest.setTotalQuantity(foodRecoveryRequestToDriver.getQuantity());
                foodSafetyCheckRequest.setMessage(foodRecoveryRequestToDriver.getMessage()+";"+txtComments.getText());
                
                foodSafetyCheckRequest.setStatus("Requested");
                foodRecoveryRequestToDriver.setStatus("Safety Check Requested");
                
                
                Organization org = null;
                for (Enterprise enterprise : network.getEnterpriseDirectory().getEnterpriseList()) {
                    if (enterprise instanceof FoodBankEnterprise) {

                         for (Organization organization : enterprise.getOrganizationDirectory().getOrganizationList()){
                             
                             if(organization instanceof FoodSafetyOfficerOrganization){
                                 org = organization;
                                 break;
                             }
                             
                         }
                    }
                }
                if (org != null) {
                    org.getWorkQueue().getWorkRequestList().add(foodSafetyCheckRequest);
                    userAccount.getWorkQueue().getWorkRequestList().add(foodSafetyCheckRequest);
                }

        populateRequestsTable();
        populateStatusTable();
        }
        else{
            JOptionPane.showMessageDialog(null, "Safety check request is already submitted");
        }


    }//GEN-LAST:event_btnRequestFoodSafetyCheckActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnRequestFoodSafetyCheck;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblUsername;
    private javax.swing.JTable tblWorkQueue;
    private javax.swing.JTable tblWorkQueue1;
    private javax.swing.JTextField txtComments;
    // End of variables declaration//GEN-END:variables
}
