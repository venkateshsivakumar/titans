/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.FoodSafetyOfficerRole;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.*;
import Business.UserAccount.UserAccount;
import Business.Warehouse.FoodItem;
import Business.Warehouse.FoodStorage;
import Business.WorkQueue.*;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author navee
 */
public class FoodSafetyCheckJPanel extends javax.swing.JPanel {
    JPanel userProcessContainer;
    Enterprise enterprise;
    Network network;
    UserAccount userAccount;
    EcoSystem ecosystem;
    FoodStorage storage;
 

    /**
     * Creates new form FoodSafetyCheckJPanel
     */
    public FoodSafetyCheckJPanel(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Network network, EcoSystem business) {
        initComponents();
        initComponents();
        this.userProcessContainer = userProcessContainer;
        this.enterprise = enterprise;
        this.network = network;
        this.userAccount = account;
        this.ecosystem = ecosystem;
        this.storage=new FoodStorage();
        lblUsername.setText(userAccount.getUsername());
        lblDate.setText((new Date()).toString());
        sliderRating.setValue(0);
        
        populateRequestsTable();
        
        populateInventoryTable();
    }
    
    public void populateInventoryTable(){
        
        
        DefaultTableModel model = (DefaultTableModel) tblFoodStorage.getModel();
        
        model.setRowCount(0);
        //for (Network net : ecosystem.getNetworkList()) {

            for (FoodItem item:storage.getFoodItemDictionary()) {
                Object[] row = new Object[4];
                row[0] = item;
                row[1] = item.getType();
                row[2] =item.getItemRating();
                row[3] = item.getExpiryDate().toString();
                 model.addRow(row);

            }
        
    }
    

    
    
    public void populateRequestsTable(){
        
        DefaultTableModel model = (DefaultTableModel) tblWorkQueue.getModel();
        
        model.setRowCount(0);
        //for (Network net : ecosystem.getNetworkList()) {

            for (Enterprise ent : network.getEnterpriseDirectory().getEnterpriseList()) {
                for(Organization org : ent.getOrganizationDirectory().getOrganizationList())
                {
                if (org instanceof FoodSafetyOfficerOrganization) {
                    for (WorkRequest request : org.getWorkQueue().getWorkRequestList()) {
                        if (request instanceof FoodSafetyCheckRequest) {
                                    Object[] row = new Object[10];
                                    row[0] = (FoodSafetyCheckRequest)request;
                                    row[1] = ((FoodSafetyCheckRequest)request).getSender();
                                    row[2] =((FoodSafetyCheckRequest)request).getReceiver() == null ? null : ((FoodSafetyCheckRequest) request).getReceiver().getEmployee().getName();
                                    row[3] = ((FoodSafetyCheckRequest)request).getRequestDate();
                                    row[4] = ((FoodSafetyCheckRequest)request).getAcceptedDate();
                                    row[5] = ((FoodSafetyCheckRequest)request).getFoodDescription();
                                    row[6] = ((FoodSafetyCheckRequest)request).getTotalQuantity();
                                    row[7] = ((FoodSafetyCheckRequest)request).getStatus();
                                    row[8] = ((FoodSafetyCheckRequest)request).getResolveDate();
                                    row[9] = ((FoodSafetyCheckRequest)request).getMessage();

                                    model.addRow(row);
                            
        }
                    }
                }
            }
            }
        
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tblWorkQueue = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblUsername = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        btnView = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtFoodDescription = new javax.swing.JTextArea();
        jLabel7 = new javax.swing.JLabel();
        txtQuantityReceived = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        dateExpiry = new com.toedter.calendar.JDateChooser();
        typeJComboBox = new javax.swing.JComboBox<>();
        txtItemQuantity = new javax.swing.JTextField();
        sliderRating = new javax.swing.JSlider();
        btnUpdateInventory = new javax.swing.JButton();
        txtItemName = new javax.swing.JTextField();
        btnAccept = new javax.swing.JButton();
        btnComplete = new javax.swing.JButton();
        jScrollPane3 = new javax.swing.JScrollPane();
        tblFoodStorage = new javax.swing.JTable();

        setBackground(new java.awt.Color(255, 255, 255));

        tblWorkQueue.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "RequestID", "Sender", "Receiver", "Requested date", "Accepted Date", "Food items", "Quantity", "Status", "Resolved Date", "Comments"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Integer.class, java.lang.String.class, java.lang.Object.class, java.lang.String.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false, true, false
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tblWorkQueue.getTableHeader().setReorderingAllowed(false);
        jScrollPane1.setViewportView(tblWorkQueue);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Food Safety Work Area");

        jLabel2.setText("Logged in as:");

        lblUsername.setFont(new java.awt.Font("Times New Roman", 0, 13)); // NOI18N
        lblUsername.setText("<username here>");

        jLabel4.setText("Date");

        lblDate.setText("<Date here>");

        btnView.setText("View Details");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        jLabel6.setText("Food Description");

        txtFoodDescription.setColumns(20);
        txtFoodDescription.setRows(5);
        jScrollPane2.setViewportView(txtFoodDescription);

        jLabel7.setText("Quantity received");

        txtQuantityReceived.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtQuantityReceivedActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Curate and Add to Food Storage");

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel9.setText("Item Name:");

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("Item Rating");

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel11.setText("Item Quantity:");

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel13.setText("Expiry Date:");

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel14.setText("Category");

        typeJComboBox.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Dairy product", "Rice/Wheat/Barley(Uncooked)", "Vegetables & Fruits", "Grocery utilities", "Baked items" }));
        typeJComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                typeJComboBoxActionPerformed(evt);
            }
        });

        txtItemQuantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtItemQuantityActionPerformed(evt);
            }
        });

        sliderRating.setMaximum(5);
        sliderRating.setMinorTickSpacing(1);
        sliderRating.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sliderRatingMouseClicked(evt);
            }
        });

        btnUpdateInventory.setText("Label and Update Inventory");
        btnUpdateInventory.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUpdateInventoryActionPerformed(evt);
            }
        });

        btnAccept.setText("Accept");
        btnAccept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAcceptActionPerformed(evt);
            }
        });

        btnComplete.setText("Complete");
        btnComplete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCompleteActionPerformed(evt);
            }
        });

        tblFoodStorage.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Item Name", "Category", "Rating", "Expiry Date"
            }
        ));
        jScrollPane3.setViewportView(tblFoodStorage);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(62, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 852, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 852, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(lblUsername, javax.swing.GroupLayout.DEFAULT_SIZE, 191, Short.MAX_VALUE)
                                .addComponent(lblDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(btnView, javax.swing.GroupLayout.PREFERRED_SIZE, 349, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txtQuantityReceived, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 127, Short.MAX_VALUE)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(dateExpiry, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtItemQuantity)
                                    .addComponent(sliderRating, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(txtItemName)))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(typeJComboBox, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
                .addContainerGap(45, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnUpdateInventory)
                .addGap(172, 172, 172))
            .addGroup(layout.createSequentialGroup()
                .addGap(431, 431, 431)
                .addComponent(btnAccept)
                .addGap(34, 34, 34)
                .addComponent(btnComplete, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(241, 241, 241)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblUsername))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblDate))
                .addGap(17, 17, 17)
                .addComponent(jLabel1)
                .addGap(31, 31, 31)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnAccept)
                    .addComponent(btnComplete))
                .addGap(49, 49, 49)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnView)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(5, 5, 5)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(31, 31, 31)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtQuantityReceived, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel9)
                            .addComponent(txtItemName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(12, 12, 12)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(typeJComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txtItemQuantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel13)
                            .addComponent(dateExpiry, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(sliderRating, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addGap(27, 27, 27)
                .addComponent(btnUpdateInventory)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 43, Short.MAX_VALUE)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 246, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(242, 242, 242))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        // TODO add your handling code here:
        
        int selectedRow = tblWorkQueue.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }
        else{
        FoodSafetyCheckRequest foodSafetyCheckRequest = (FoodSafetyCheckRequest) tblWorkQueue.getValueAt(selectedRow, 0);
        
        txtFoodDescription.setText(foodSafetyCheckRequest.getFoodDescription());
        txtQuantityReceived.setText(foodSafetyCheckRequest.getTotalQuantity());
        }
    }//GEN-LAST:event_btnViewActionPerformed

    private void txtQuantityReceivedActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtQuantityReceivedActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtQuantityReceivedActionPerformed

    private void typeJComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_typeJComboBoxActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_typeJComboBoxActionPerformed

    private void txtItemQuantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtItemQuantityActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtItemQuantityActionPerformed

    private void btnUpdateInventoryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUpdateInventoryActionPerformed
        // TODO add your handling code here:
        try{
        String itemName=txtItemName.getText();
        String category = typeJComboBox.getSelectedItem().toString();
        
        double quantityReceived=Double.parseDouble(txtQuantityReceived.getText());

        double itemQuantity = Double.parseDouble(txtItemQuantity.getText());
        
        

        Date expirDate = dateExpiry.getDate();
        int itemRating= sliderRating.getValue();
        
        //validation
        if(itemName.equals("")||category.equals("")||itemQuantity<=0||itemQuantity>quantityReceived||expirDate==null||itemRating==0){
            JOptionPane.showMessageDialog(null, "Please enter data correctly");
        }
        else{
            
            FoodItem item = new FoodItem();
            item.setItemName(itemName);
            item.setType(category);
            item.setQuantity(itemQuantity);
            item.setCuratedOn(new Date());
            item.setExpiryDate(expirDate);
            item.setItemRating(itemRating);
            
            FoodStorage storageUnit = new FoodStorage();
            storageUnit.getFoodItemDictionary().add(item);
            
            JOptionPane.showMessageDialog(null, "Successfully added");
            
        txtItemName.setText("");
        typeJComboBox.setSelectedIndex(0);
        txtItemQuantity.setText("");
        dateExpiry.setDate(null);
        sliderRating.setValue(0);
        
        populateInventoryTable();
            
            
        }
        }
        catch(NumberFormatException ex){
            
            JOptionPane.showMessageDialog(null, "please enter a valid number");
            
        }
        
        
    }//GEN-LAST:event_btnUpdateInventoryActionPerformed

    private void sliderRatingMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sliderRatingMouseClicked
 
    }//GEN-LAST:event_sliderRatingMouseClicked

    private void btnAcceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAcceptActionPerformed
        // TODO add your handling code here:
         int selectedRow = tblWorkQueue.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }
        else{
        FoodSafetyCheckRequest foodSafetyCheckRequest = (FoodSafetyCheckRequest) tblWorkQueue.getValueAt(selectedRow, 0);
        foodSafetyCheckRequest.setReceiver(userAccount);
        foodSafetyCheckRequest.setAcceptedDate(new Date());
        foodSafetyCheckRequest.setResolveDate(null);
        foodSafetyCheckRequest.setStatus("Accepted");
        populateRequestsTable();
        
        }
        
    }//GEN-LAST:event_btnAcceptActionPerformed

    private void btnCompleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCompleteActionPerformed
        // TODO add your handling code here:
        int selectedRow = tblWorkQueue.getSelectedRow();

        if (selectedRow < 0) {
            JOptionPane.showMessageDialog(null, "Please select a row");
            return;
        }
        else{
        FoodSafetyCheckRequest foodSafetyCheckRequest = (FoodSafetyCheckRequest) tblWorkQueue.getValueAt(selectedRow, 0);
        if("Accepted".equalsIgnoreCase(foodSafetyCheckRequest.getStatus())){
            foodSafetyCheckRequest.setReceiver(userAccount);
            foodSafetyCheckRequest.setResolveDate(new Date());
            foodSafetyCheckRequest.setStatus("completed");
            populateRequestsTable();
        }
        else{
            JOptionPane.showMessageDialog(null, "Cant Complete before accepting");
        }
        }
    }//GEN-LAST:event_btnCompleteActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAccept;
    private javax.swing.JButton btnComplete;
    private javax.swing.JButton btnUpdateInventory;
    private javax.swing.JButton btnView;
    private com.toedter.calendar.JDateChooser dateExpiry;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblUsername;
    private javax.swing.JSlider sliderRating;
    private javax.swing.JTable tblFoodStorage;
    private javax.swing.JTable tblWorkQueue;
    private javax.swing.JTextArea txtFoodDescription;
    private javax.swing.JTextField txtItemName;
    private javax.swing.JTextField txtItemQuantity;
    private javax.swing.JTextField txtQuantityReceived;
    private javax.swing.JComboBox<String> typeJComboBox;
    // End of variables declaration//GEN-END:variables
}
