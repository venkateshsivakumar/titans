/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.DistributionCenterOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.MealServed.MealServedJPanel;
import javax.swing.JPanel;

/**
 *
 * @author Kaviya
 */
public class DistributionCenterRole extends Role {
     @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Network network, EcoSystem business) {
        return new MealServedJPanel(userProcessContainer,account,(DistributionCenterOrganization)organization,enterprise,network); //To change body of generated methods, choose Tools | Templates.
    }

    
}
