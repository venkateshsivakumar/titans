/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Role;

import Business.EcoSystem;
import Business.Enterprise.Enterprise;
import Business.Network.Network;
import Business.Organization.LogisticsOrganization;
import Business.Organization.Organization;
import Business.UserAccount.UserAccount;
import UserInterface.DonatorRole.DonateFoodWorkAreaJPanel;
import UserInterface.LogisticsRole.LogisticsWorkAreaJPanel;
import javax.swing.JPanel;

/**
 *
 * @author venka
 */
public class LogisticsRole extends Role{
    @Override
    public JPanel createWorkArea(JPanel userProcessContainer, UserAccount account, Organization organization, Enterprise enterprise, Network network, EcoSystem business) {
        return new LogisticsWorkAreaJPanel(userProcessContainer,account,(LogisticsOrganization)organization,enterprise,network); //To change body of generated methods, choose Tools | Templates.
    }
}
