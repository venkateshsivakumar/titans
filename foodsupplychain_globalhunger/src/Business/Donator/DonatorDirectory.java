/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Donator;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author navee
 */
public class DonatorDirectory {
    
    private ArrayList<Donator> donorList;

    public DonatorDirectory() {
        donorList = new ArrayList();
    }

    public ArrayList<Donator> getChildList() {
        return donorList;
    }
    
    public Donator addDonator(Donator donator){;
        donorList.add(donator);
        return donator;
    }
    
}
