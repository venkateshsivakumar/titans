/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.Date;


/**
 *
 * @author navee
 */
public class FoodDeliveryRequestToLogistics extends FoodRequestByDCWorkReq{
     

    private Date acceptedDate;


  
    public Date getAcceptedDate() {
        return acceptedDate;
    }

    public void setAcceptedDate(Date acceptedDate) {
        this.acceptedDate = acceptedDate;
    }

 
    
     @Override
    public String toString() {
       return String.valueOf(getRequestId());
       }
    
    
    
}
