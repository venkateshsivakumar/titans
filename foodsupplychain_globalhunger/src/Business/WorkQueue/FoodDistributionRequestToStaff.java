/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author venka
 */
public class FoodDistributionRequestToStaff extends FoodRequestByDCWorkReq {
    
    private String address;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
     @Override
    public String toString() {
       return String.valueOf(getStatus());
       }
}
