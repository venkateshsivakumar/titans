/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

/**
 *
 * @author Kaviya
 */
public class FoodDistReqtoStaff extends WorkRequest{
    private String mealtype;
    private int noofmeals;
    private String status;
    
    public int getNoofmeals() {
        return noofmeals;
    }

    public void setNoofmeals(int noofmeals) {
        this.noofmeals = noofmeals;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMealtype() {
        return mealtype;
    }

    public void setMealtype(String mealtype) {
        this.mealtype = mealtype;
    }
    
    @Override
    public String toString(){
        return mealtype;
    }
}
