/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import java.util.Date;

/**
 *
 * @author Kaviya
 */
public class FoodRequestByDCWorkReq extends WorkRequest{
    
    
    private Date requestingDate;
    private String mealtype;
    private int noofmeals;
    private String loc;

    private String entname;

    public String getEntname() {
        return entname;
    }

    public void setEntname(String entname) {
        this.entname = entname;
    }
   
    public Date getRequestingDate() {
        return requestingDate;
    }

    public void setRequestingDate(Date requestingDate) {
        this.requestingDate = requestingDate;
    }

    public String getMealtype() {
        return mealtype;
    }

    public void setMealtype(String mealtype) {
        this.mealtype = mealtype;
    }

    public int getNoofmeals() {
        return noofmeals;
    }

    public void setNoofmeals(int noofmeals) {
        this.noofmeals = noofmeals;
    }

    public String getLoc() {
        return loc;
    }

    public void setLoc(String loc) {
        this.loc = loc;
    }
    
     @Override
    public String toString() {
       return String.valueOf(getRequestId());
       }
}
