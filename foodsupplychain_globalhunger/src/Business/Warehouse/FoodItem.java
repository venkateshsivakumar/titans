/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Warehouse;

import java.util.Date;

/**
 *
 * @author Titan
 */
public class FoodItem {
    private static int count=0;
    private int foodItemId;
    private String itemName;
    private int itemRating;
    private double quantity;
    private Date curatedOn;
    private Date expiryDate;
    private String type;

    public FoodItem() {

        this.curatedOn = new Date();
        this.foodItemId =count++;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public int getFoodItemId() {
        return foodItemId;
    }

    public void setFoodItemId(int foodItemId) {
        this.foodItemId = foodItemId;
    }

    public int getItemRating() {
        return itemRating;
    }

    public void setItemRating(int itemRating) {
        this.itemRating = itemRating;
    }

    public Date getCuratedOn() {
        return curatedOn;
    }

    public void setCuratedOn(Date curatedOn) {
        this.curatedOn = curatedOn;
    }

    

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
    
    @Override
    public String toString(){
        return this.getItemName();
    }
    
    
    
    
}
