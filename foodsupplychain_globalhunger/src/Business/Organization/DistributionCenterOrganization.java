/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DistributionCenterRole;
import Business.Role.Role;
import java.util.ArrayList;
/**
 *
 * @author Kaviya
 */
public class DistributionCenterOrganization extends Organization{
     public DistributionCenterOrganization() {
        super(Type.DistributionStaff.getValue());
    }
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new DistributionCenterRole());
        return roles;
    }


}
