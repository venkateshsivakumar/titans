/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DonatorRole;
import Business.Role.Role;
import java.util.ArrayList;

/**
 *
 * @author navee
 */
public class DonatorOrganization extends Organization{

    public DonatorOrganization() {
        super(Type.Donator.getValue());
    }
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new DonatorRole());
        return roles;
    }
    
}
