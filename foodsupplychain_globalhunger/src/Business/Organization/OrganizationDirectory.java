/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author Titan
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.FoodSafety.getValue())){
            organization = new FoodSafetyOfficerOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Donator.getValue())){
           organization = new DonatorOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.LogisticsStaff.getValue())){
            organization = new LogisticsOrganization();
            organizationList.add(organization);
        }
        else if(type.getValue().equals(Type.DistributionStaff.getValue())){
            organization = new DistributionCenterOrganization();
            organizationList.add(organization);
        }
        
        return organization;
    }
}