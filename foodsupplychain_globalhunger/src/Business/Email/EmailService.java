/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Email;

import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import javax.swing.JOptionPane;

/**
 *
 * @author venka
 */
public class EmailService {
    
    public static void sendMail( String userName, String password){  
                        String email = userName;
                        System.out.println(email);
                        String to = email;
                        String from = "Sysadmin";
                        String host = "smtp.gmail.com";
                        Properties properties = new Properties();
                        properties.put("mail.smtp.auth", "true");
                        properties.put("mail.smtp.starttls.enable", "true");
                        properties.put("mail.smtp.host", host);
                        properties.put("mail.smtp.port", "587");
                        properties.put("mail.smtp.ssl.trust", "smtp.gmail.com");
                        Session session = Session.getInstance(properties,
                                new javax.mail.Authenticator() {
                                    protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                                        return new javax.mail.PasswordAuthentication("logisticsstaff1@gmail.com", "ls1@gmail");
                                    }
                                }
                            );    
                        try {
                            MimeMessage message = new MimeMessage(session);
                            message.setFrom(new InternetAddress(from));
                            message.setRecipient(Message.RecipientType.TO, new InternetAddress(to));
                            message.setSubject("Credential details");
                            message.setText("HELLO " + "\n\nPlease find the login credentials " + "\nuser id: " + userName + "\n\nfor password:" + password + "\n\n\n\n**This is an automated message please do not reply**");

                            javax.mail.Transport.send(message);
                            JOptionPane.showMessageDialog(null, "The message has been sent ");
                             

                        } catch (Exception e) {
                            JOptionPane.showMessageDialog(null, e);
                        }  
                   
        }
    }
    

