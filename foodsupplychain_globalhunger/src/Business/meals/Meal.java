/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.meals;

import java.util.Date;

/**
 *
 * @author Kaviya
 */
public class Meal {
    
    private int id;
    private String name;
    private int age;
    private int noofmeals;
    private Date dateServed;
    private static int count = 1;

    public Meal() {
        id = count;
        count++;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getNoofmeals() {
        return noofmeals;
    }

    public void setNoofmeals(int noofmeals) {
        this.noofmeals = noofmeals;
    }

    public Date getDateServed() {
        return dateServed;
    }

    public void setDateServed(Date dateServed) {
        this.dateServed = dateServed;
    }

    

   @Override
    public String toString() {
        return name;
    }

}
