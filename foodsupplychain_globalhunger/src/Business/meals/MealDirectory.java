/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.meals;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Kaviya
 */
public class MealDirectory {
   private ArrayList<Meal> MealList;

    public ArrayList<Meal> getMealList() {
        return MealList;
    }

    public void setDCList(ArrayList<Meal> MealList) {
        this.MealList = MealList;
    }
    
     public Meal addMeal(String name,int noofmeal, int age, Date servedDate){
        Meal meal = new Meal();
        meal.setName(name);
        meal.setAge(age);  
        meal.setNoofmeals(noofmeal);
        meal.setDateServed(servedDate);
        MealList.add(meal);
        return meal;
    }
 
}
